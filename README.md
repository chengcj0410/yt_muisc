[功能描述]：  
使用youtube api將喜愛的Youtube影片連結紀錄成清單，以便後續可直接觀賞。  

[開發環境]：  
1.Spring boot:2.4.4  
2.Java Version:1.8  
3.Thymeleaf:3.0.4  
4.Apache Tomcat:9.0.44  
5.[Youtube api:v3-rev222-1.25.0](https://console.cloud.google.com/apis/library/youtube.googleapis.com?id=125bab65-cfb6-4f25-9826-4dcc309bc508&project=api-project-415597704372)  
6.[IDE :STS 4](https://spring.io/tools/)  
7.[DB :MariaDB 10.5](https://downloads.mariadb.org/)  

