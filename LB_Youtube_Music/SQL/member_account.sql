-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 伺服器版本:                        10.5.9-MariaDB - mariadb.org binary distribution
-- 伺服器作業系統:                      Win64
-- HeidiSQL 版本:                  11.0.0.6062
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 傾印 youtubemusiclist 的資料庫結構
CREATE DATABASE IF NOT EXISTS `youtubemusiclist` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `youtubemusiclist`;

-- 傾印  資料表 youtubemusiclist.hibernate_sequence 結構
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在傾印表格  youtubemusiclist.hibernate_sequence 的資料：~0 rows (近似值)
DELETE FROM `hibernate_sequence`;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES
	(6);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;

-- 傾印  資料表 youtubemusiclist.member_account 結構
CREATE TABLE IF NOT EXISTS `member_account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='會員資料表';

-- 正在傾印表格  youtubemusiclist.member_account 的資料：~1 rows (近似值)
DELETE FROM `member_account`;
/*!40000 ALTER TABLE `member_account` DISABLE KEYS */;
INSERT INTO `member_account` (`id`, `email`, `password`, `name`, `account`, `create_date`) VALUES
	(1, 'aaa@gmail', 'lb', 'LB', 'LB', '2021-05-12 14:55:36');
/*!40000 ALTER TABLE `member_account` ENABLE KEYS */;

-- 傾印  資料表 youtubemusiclist.video_info 結構
CREATE TABLE IF NOT EXISTS `video_info` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(20) unsigned NOT NULL,
  `youtube_id` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `singer` varchar(100) DEFAULT NULL,
  `channel_title` varchar(50) DEFAULT NULL,
  `start_time` int(11) DEFAULT NULL,
  `total_time` int(11) DEFAULT NULL,
  `add_time` datetime DEFAULT NULL,
  `thumbnail_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`video_id`),
  KEY `FK_video_info_video_list` (`list_id`),
  CONSTRAINT `FK_video_info_video_list` FOREIGN KEY (`list_id`) REFERENCES `video_list` (`list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- 正在傾印表格  youtubemusiclist.video_info 的資料：~7 rows (近似值)
DELETE FROM `video_info`;
/*!40000 ALTER TABLE `video_info` DISABLE KEYS */;
INSERT INTO `video_info` (`video_id`, `list_id`, `youtube_id`, `title`, `singer`, `channel_title`, `start_time`, `total_time`, `add_time`, `thumbnail_url`) VALUES
	(58, 37, 'XBmvbhiLADY', '【Resident Evil Village】yes yes', 'Gawr Gura Ch. hololive-EN', 'Gawr Gura Ch. hololive-EN', 0, 0, '2021-05-18 02:05:23', 'https://yt3.ggpht.com/ytc/AAUvwnhSSaF3Q-PyyTSis4EH6Cu8FZ32LNvkxI9Gl_rn=s88-c-k-c0x00ffffff-no-rj'),
	(59, 37, '0A57ZgkSCSw', '美しく感動するかっこいい曲アニメEDメドレー【20曲】Most Beautiful Anime Endings Full Songs Compilation', 'Juna Chan', 'Juna Chan', 0, 0, '2021-05-18 08:10:50', 'https://yt3.ggpht.com/ytc/AAUvwng5QXuRIlChLNktnGSwVYMbXY-5RoD0NCUjtFl2zA=s88-c-k-c0x00ffffff-no-rj'),
	(60, 37, '0A57ZgkSCSw', '美しく感動するかっこいい曲アニメEDメドレー【20曲】', 'Juna Chan', 'Juna Chan', 0, 0, '2021-05-18 08:23:04', 'https://yt3.ggpht.com/ytc/AAUvwng5QXuRIlChLNktnGSwVYMbXY-5RoD0NCUjtFl2zA=s88-c-k-c0x00ffffff-no-rj'),
	(61, 37, 'd9EcD3_1LGQ', '【大型DLCきました】新☆拡張コンテンツ「ドルイドの怒り」を遊んでみよう！！【アサシンクリード ヴァルハラ】', 'Korone Ch. 戌神ころね', 'Korone Ch. 戌神ころね', 240, 300, '2021-05-18 11:43:53', 'https://yt3.ggpht.com/ytc/AAUvwnj7QwmJ9YhyKY-9SkOKhKIzTKDMJp0HX2vAdQUVdw=s88-c-k-c0x00ffffff-no-rj'),
	(62, 37, '5m0D2R3AUow', '86－不存在的戰區－[ ED ] - 「SawanoHiroyuki[nZk]:mizuki - Avid」', 'Neet music channel', 'Neet music channel', 0, 0, '2021-05-18 11:44:37', 'https://yt3.ggpht.com/ytc/AAUvwnhqD-kR1xlLQav5YQQenSGgJAuDOgDihwysJt7VYA=s88-c-k-c0x00ffffff-no-rj'),
	(63, 37, 'KC69x9JHqaE', '【360°動画】 命ばっかり by 燦鳥ノム【歌ってみた】', '燦鳥ノム - SUNTORY NOMU -', '燦鳥ノム - SUNTORY NOMU -', 0, 0, '2021-05-18 11:44:57', 'https://yt3.ggpht.com/ytc/AAUvwniBLLS-wyCwhcXqMvqPh2rbO9rR2-SEFSEGbFI-qA=s88-c-k-c0x00ffffff-no-rj'),
	(64, 37, 'ykcVw8rRH8k', '本週我在聽#144｜20210518', 'KAZBOM', 'KAZBOM', 0, 0, '2021-05-18 11:58:59', 'https://yt3.ggpht.com/ytc/AAUvwniUqg-69tn6JZrfg71RLtOZ_PmDhD6EqevsRy03nJk=s88-c-k-c0x00ffffff-no-rj');
/*!40000 ALTER TABLE `video_info` ENABLE KEYS */;

-- 傾印  資料表 youtubemusiclist.video_list 結構
CREATE TABLE IF NOT EXISTS `video_list` (
  `member_id` int(11) unsigned NOT NULL,
  `list_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `list_name` varchar(50) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `list_comment` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`list_id`) USING BTREE,
  KEY `member_id` (`member_id`),
  CONSTRAINT `FK_video_list_member_account` FOREIGN KEY (`member_id`) REFERENCES `member_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- 正在傾印表格  youtubemusiclist.video_list 的資料：~1 rows (近似值)
DELETE FROM `video_list`;
/*!40000 ALTER TABLE `video_list` DISABLE KEYS */;
INSERT INTO `video_list` (`member_id`, `list_id`, `list_name`, `create_at`, `update_at`, `list_comment`) VALUES
	(1, 37, 'test1', '2021-05-18 02:03:45', '2021-05-18 02:03:45', '測試用');
/*!40000 ALTER TABLE `video_list` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
