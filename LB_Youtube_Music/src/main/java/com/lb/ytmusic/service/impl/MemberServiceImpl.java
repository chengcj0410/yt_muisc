package com.lb.ytmusic.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lb.ytmusic.dao.MemberDao;
import com.lb.ytmusic.entity.MemberInfo;
import com.lb.ytmusic.service.MemberInfoService;

@Service
public class MemberServiceImpl implements MemberInfoService {

	private static Logger logger = LogManager.getLogger(MemberServiceImpl.class);

	@Autowired
	private MemberDao memberDao;

	public List<MemberInfo> findAll() {

		if (logger.isDebugEnabled()) {
			logger.debug("getALLUserAccount: {}" + findAll().get(0).getAccount());
		}

		return memberDao.findAll();

	}

	@Override
	public List<MemberInfo> findByEmail(String email) {
		if (logger.isDebugEnabled()) {
			logger.debug("getByUserEmail: {}", email);
		}
		return memberDao.findByEmail(email);

	}

	@Override
	public List<MemberInfo> findByName(String name) {
		if (logger.isDebugEnabled()) {
			logger.debug("getByUserName: {}", name);
		}
		return memberDao.findByName(name);

	}

	@Override
	public MemberInfo findByAccount(String account) {
		if (logger.isDebugEnabled()) {
			logger.debug("getByUserAccount: {}", account);
		}
		if (memberDao.findByAccount(account) != null) {
			return memberDao.findByAccount(account);
		}
		
		return null;
	}

	@Override
	public List<MemberInfo> findAllById(Long id) {

		if (logger.isDebugEnabled()) {
			logger.debug("getByUserId: {}", id);
		}

		return memberDao.findAll();
	}

	@Override
	public boolean save(String name, String account, String password, String email) {
		try {
			if(memberDao.findByName(name) !=null || memberDao.findByAccount(account)!=null) {
				logger.info(">>>logbaccck failed<<<");
				return false;
			}
					
			MemberInfo mi = new MemberInfo();
			mi.setAccount(account);
			mi.setEmail(email);
			mi.setName(name);
			mi.setPassword(password);
			mi.setCreatedate(new Date());
			memberDao.save(mi);
			logger.info(">>>logback success<<<");
			
			return true;
		} catch (Exception e) {
			logger.error("save failed. ex: ", e);
			return false;
		}
	}


}
