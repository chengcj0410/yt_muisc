package com.lb.ytmusic.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.lb.ytmusic.dao.VideoListDao;
import com.lb.ytmusic.entity.VideoList;
import com.lb.ytmusic.model.VideoListBo;
import com.lb.ytmusic.service.VideoListService;

@Service
public class VideoListServiceImpl implements VideoListService {

	private static Logger logger = LogManager.getLogger(VideoInfoServiceImpl.class);

	@Autowired
	private VideoListDao videolistdao;

	@Override
	public List<VideoList> findAll() {

		if (logger.isDebugEnabled()) {
			logger.debug("getALLVideoListID: {}" + findAll().get(0).getListId());
		}

		return videolistdao.findAll();

	}

	@Override
	public void add(VideoListBo bo) {
		VideoList vl = new VideoList();
		try {
			vl.setMemberId(bo.getMemberId());
			vl.setListName(bo.getListName());
			vl.setCreate_at(new Date());
			vl.setUpdate_at(new Date());
			videolistdao.save(vl);

		} catch (Exception e) {
			Log.debug("insert failed" + e); // TODO: handle exception
		}
		Log.info("insert successed"); // TODO: handle exception
	}

	@Override
	public List<VideoList> findlistbyid(Long memberId) {
		
		if (logger.isDebugEnabled()) {
			logger.debug("getALLVideoListID: {}" + memberId);
		}

		return videolistdao.findlistbyid(memberId);
	}

	@Override
	public VideoListBo findlistnamebyid(Long list_id) {
		if (logger.isDebugEnabled()) {
			logger.debug("findlistnamebyid: {}" + findlistnamebyid(list_id).getListName());
		}
		VideoListBo bo = new VideoListBo();
		DateFormat df = new SimpleDateFormat("MMM dd, yyyy");

		try {
			VideoList entity = videolistdao.findlistnamebyid(list_id);
			bo.setCreate_at(df.format(entity.getCreate_at()));
			bo.setList_comment(entity.getList_comment());
			bo.setListId(entity.getListId());
			bo.setListName(entity.getListName());
			bo.setMemberId(entity.getMemberId());
			bo.setUpdate_at(df.format(entity.getUpdate_at()));
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}

		return bo;

	}

	@Override
	public Page<VideoList> getPageVideoList(Long id, int page, int size) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by("listId").ascending());

			Page<VideoList> pageResult = videolistdao.findListbyMemberID(id, pageable);
			return pageResult;
		} catch (Exception e) {
			Log.debug("getPageVideoList{}" + e); // TODO: handle exception
			return null;
		}
	}

	@Override
	public void remove(Long listId) {

		try {
			VideoList entity = videolistdao.findlistnamebyid(listId);
			Log.debug("findlistnamebyid entity is{}" + entity.getListName());

			videolistdao.deleteById(listId);
			
			Log.info("remove video_id " + listId + " success");
		} catch (Exception e) {
			Log.debug("deleteById faild{}" + e);
		}

	}

	@Override
	public void editVideoComment(Long memberId,Long listId, String comment) {
		
		try {
			VideoList v_list = videolistdao.findlistnamebyid(listId);
			v_list.setList_comment(comment);
			v_list.setUpdate_at(new Date());
			videolistdao.save(v_list);
				
		} catch (Exception e) {
			// TODO: handle exception
			Log.debug("editVideoComment faild{}"+e);
		}
		
	}

}
