package com.lb.ytmusic.service;

import java.util.List;

import com.lb.ytmusic.entity.MemberInfo;


public interface MemberInfoService  {

	public List<MemberInfo> findAllById(Long userId);

	public List<MemberInfo> findAll();

	public List<MemberInfo> findByEmail(String email);

	public List<MemberInfo> findByName(String name);

	public MemberInfo findByAccount(String account);
	
	public boolean save(String name,String account,String password,String email);
	
	
}
