package com.lb.ytmusic.service;

import java.util.List;

import org.springframework.data.domain.Page;
import com.lb.ytmusic.entity.VideoInfo;
import com.lb.ytmusic.model.VideoInfoBo;

public interface VideoInfoService {

//	public List<VideoInfo> findAll();

	public void add(VideoInfoBo bo);

	public VideoInfoBo findVideoById(Long video_id);

	public List<VideoInfo> findAllVideoByListId(long list_id);

	public Page<VideoInfo> findAllVideoByListIdforPage(Long id, int page, int size);

	public void remove(Long video_id);
	
	public Page<VideoInfo> findAllVideo(int page, int size);
}
