package com.lb.ytmusic.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.lb.ytmusic.entity.VideoList;
import com.lb.ytmusic.model.VideoListBo;

public interface VideoListService {

	public List<VideoList> findAll();

	public void add(VideoListBo bo);

	public List<VideoList> findlistbyid(Long memberId);

	public VideoListBo findlistnamebyid(Long list_id);

	Page<VideoList> getPageVideoList(Long id, int page, int size);
	
	public void remove(Long listId);
	
	public void editVideoComment(Long memberId,Long listId,String comment);
}
