package com.lb.ytmusic.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.lb.ytmusic.dao.VideoInfoDao;
import com.lb.ytmusic.entity.VideoInfo;
import com.lb.ytmusic.model.VideoInfoBo;
import com.lb.ytmusic.service.VideoInfoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class VideoInfoServiceImpl implements VideoInfoService {

	private static Logger logger = LogManager.getLogger(VideoInfoServiceImpl.class);

	@Autowired
	private VideoInfoDao videoinfoDao;

	@Override
	public void add(VideoInfoBo bo) {

		try {
			VideoInfo entity = new VideoInfo();
			entity.setList_id(bo.getList_id());
			logger.info("list id is " + bo.getList_id());
			entity.setYoutube_id(bo.getYoutube_id());
			entity.setTitle(bo.getTitle());
			entity.setSinger(bo.getSinger());
			entity.setChannel_title(bo.getChannel_title());
			entity.setStart_time(bo.getStart_time());
			entity.setThumbnailUrl(bo.getThumbnailUrl());
			entity.setTotal_time(bo.getTotal_time());
			entity.setAdd_time(new Date());
			videoinfoDao.save(entity);
		} catch (Exception e) {
			logger.error("save failed. ex: ", e);
		}

	}

	@Override
	public VideoInfoBo findVideoById(Long id) {
		if (logger.isDebugEnabled()) {
			logger.debug("findVideoById: {}", id);
		}

		VideoInfoBo bo = new VideoInfoBo();
		try {

			VideoInfo entity = videoinfoDao.findVideoById(id);
			bo.setChannel_title(entity.getChannel_title());
			bo.setList_id(entity.getList_id());
			bo.setSinger(entity.getSinger());
			bo.setStart_time(entity.getStart_time());
			bo.setThumbnailUrl(entity.getThumbnailUrl());
			bo.setTotal_time(entity.getTotal_time());
			bo.setTitle(entity.getTitle());
			bo.setYoutube_id(entity.getYoutube_id());
			return bo;
		} catch (Exception e) {
			logger.error("findVideoById failed. ex: ", e);
			return null;
		}

	}

	@Override
	public List<VideoInfo> findAllVideoByListId(long list_id) {

		if (logger.isDebugEnabled()) {
			logger.debug("findAllVideoByListId: {}" + list_id);
		}
		try {
			if (videoinfoDao.findAllVideoByListId(list_id).isEmpty()) {
				return null;
			} else {
				return videoinfoDao.findAllVideoByListId(list_id);
			}
		} catch (Exception e) {
			logger.error("findAllVideoByListId failed. ex: ", e);
			return null;
		}
	}

	@Override
	public Page<VideoInfo> findAllVideoByListIdforPage(Long id, int page, int size) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by("add_time").descending());

			Page<VideoInfo> pageResult = videoinfoDao.findAllVideoByListId(id, pageable);

			return pageResult;
		} catch (Exception e) {
			Log.debug("getPageVideoList{}" + e); // TODO: handle exception
			return null;
		}

	}

	@Override
	public void remove(Long video_id) {

		try {
			VideoInfo entity = videoinfoDao.findById(video_id).orElse(new VideoInfo());
			Log.debug("deleteById entity is{}" + entity.getList_id());

			videoinfoDao.deleteById(video_id);
			Log.info("remove video_id " + video_id + " success");
		} catch (Exception e) {
			Log.debug("deleteById faild{}" + e);
		}

	}

	@Override
	public Page<VideoInfo> findAllVideo(int page, int size) {
		try {
			Pageable pageable = PageRequest.of(page, size, Sort.by("add_time").descending());

			Page<VideoInfo> pageResult = videoinfoDao.findAllVideo(pageable);

			return pageResult;
		} catch (Exception e) {
			Log.debug("findAllVideo{}" + e); // TODO: handle exception
			return null;
		}

	}

}
