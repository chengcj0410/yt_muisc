package com.lb.ytmusic.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.lb.ytmusic.model.YouTubeVideo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class YoutubeService {

	@Autowired
	private Environment env;

	/**
	 * 
	 * @param insert youtube id
	 * @return video's item by YouTubeVideo object
	 */
	public YouTubeVideo fetchVideosByQuery(String queryTerm) {
//		String[] v_url = queryTerm.split("=");
		YouTubeVideo videos = new YouTubeVideo();
		try {
			YouTube youtube = getYouTube();

			YouTube.Videos.List search = youtube.videos().list("id,snippet,contentDetails");

//			String apiKey = "AIzaSyAQEzE29Yzenk8RuMP4LafYpeICtezGkug";

			String apiKey = env.getProperty("youtube.apikey");

			// set api key
			search.setKey(apiKey);

			// System.out.print("-------------------->>ID: " + queryTerm);

			// we only want video results
			search.setId(queryTerm);

			// set the fields that we're going to use

			DateFormat df = new SimpleDateFormat("MMM dd, yyyy");

			// perform the search and parse the results
			VideoListResponse searchResponse = search.execute();
			List<Video> searchResultList = searchResponse.getItems();

			// System.out.print("Video搜尋的結果數量>>>"+searchResultList.size());

			// for (Video e : searchResultList) {
			// System.out.print("----------------------Video Search result :" + e + "\n");
			// }

			/**
			 * set data to video
			 */
			if (searchResultList != null) {
				for (Video result : searchResultList) {
					YouTube.Channels.List searchBychannels = youtube.channels().list("snippet");
					searchBychannels.setKey(apiKey);

					searchBychannels.setId(result.getSnippet().getChannelId());
					ChannelListResponse searchChannelResponse = searchBychannels.execute();
					List<Channel> searchChannelResultList = searchChannelResponse.getItems();
					// System.out.print("Channels搜尋的結果數量>>>"+searchChannelResultList.size());

					videos.setTitle(result.getSnippet().getTitle());
					videos.setUrl(buildVideoUrl(queryTerm));
					videos.setId(queryTerm);
					videos.setThumbnailUrl(result.getSnippet().getThumbnails().getDefault().getUrl());
					videos.setDescription(result.getSnippet().getDescription());
					videos.setChannelTitle(result.getSnippet().getChannelTitle());
					videos.setChannelId(buildChannelUrl(result.getSnippet().getChannelId()));
					videos.setChannelImg(
							searchChannelResultList.get(0).getSnippet().getThumbnails().getDefault().getUrl());
//					videos.setDuration(durationSwtichToTime(result.getContentDetails().getDuration()));

					// parse the date
					DateTime dateTime = result.getSnippet().getPublishedAt();
					Date date = new Date(dateTime.getValue());
					String dateString = df.format(date);

					videos.setPublishDate(dateString);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return videos;

	}

	/**
	 * Instantiates the YouTube object
	 */
	private YouTube getYouTube() {
		YouTube youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), (reqeust) -> {
		}).setApplicationName("youtube-spring-boot-demo").build();

		return youtube;
	}

	/**
	 * Constructs the URL to play the YouTube video
	 */
	private String buildVideoUrl(String videoId) {
		StringBuilder builder = new StringBuilder();
		builder.append("https://www.youtube.com/watch?v=");
		builder.append(videoId);

		return builder.toString();
	}

	public String buildVideoUrltoEmbed(String videoUrl, int starttime, int endtime) {
		StringBuilder builder = new StringBuilder();
		// String[] v_url = videoUrl.split("=");
		int v_start_time = starttime;
		int v_end_time = endtime;

		builder.append("https://www.youtube.com/embed/").append(videoUrl).append("?start=").append(v_start_time)
				.append("&end=").append(v_end_time).append("&autoplay=1");

		// System.out.print(builder + "\t");

		return builder.toString();
	}

	/**
	 * 
	 * @param videotime_h
	 * @param videotime_m
	 * @param videotime_s
	 * @return https://www.youtube.com/channel/UC1DCedRgGHBdm81E1llLhOQ
	 */
	public String buildChannelUrl(String channelId) {
		StringBuilder builder = new StringBuilder();

		builder.append("https://www.youtube.com/channel/").append(channelId);
		// System.out.print(builder + "\t");

		return builder.toString();
	}

	public int timeToSecond(int videotime_h, int videotime_m, int videotime_s) {
		Log.info("addvideo的hour{}" + videotime_h);
		Log.info("addvideo的min{}" + videotime_m);
		Log.info("addvideo的sec{}" + videotime_s);
		int totaltime = 0;
//		int hour = videotime_h;
//		int min = videotime_m;
//		int sec = videotime_s;

		try {
			totaltime = (videotime_h * 60 * 60) + (videotime_m * 60) + videotime_s;

		} catch (Exception e) {
			// TODO: handle exception
			return totaltime;
		}

		return totaltime;
	}

}
