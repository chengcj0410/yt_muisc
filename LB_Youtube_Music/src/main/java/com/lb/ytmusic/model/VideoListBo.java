package com.lb.ytmusic.model;


import lombok.Data;
import lombok.ToString;


@ToString
@Data
public class VideoListBo {

	private long memberId;

	private long listId;

	private String listName;

	private String create_at;

	private String update_at;
	
	private String list_comment;

	public long getMemberId() {
		return memberId;
	}

	public String getList_comment() {
		return list_comment;
	}

	public void setList_comment(String list_comment) {
		this.list_comment = list_comment;
	}

	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}

	
	public long getListId() {
		return listId;
	}

	public void setListId(long listId) {
		this.listId = listId;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	public String getCreate_at() {
		return create_at;
	}

	public void setCreate_at(String create_at) {
		this.create_at = create_at;
	}

	public String getUpdate_at() {
		return update_at;
	}

	public void setUpdate_at(String update_at) {
		this.update_at = update_at;
	}
	
}
