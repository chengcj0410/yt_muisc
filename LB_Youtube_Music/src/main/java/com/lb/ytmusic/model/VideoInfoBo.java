package com.lb.ytmusic.model;

import java.util.Date;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class VideoInfoBo {

	private long video_id;
	private long list_id;
	private String youtube_id;
	private String title;
	private String singer;
	private String channel_title;
	private int start_time;
	private String thumbnailUrl;
	private int total_time;
	private Date add_time;
	private int time_h;
	private int time_m;
	private int time_s;

	public int getTime_h() {
		return time_h;
	}

	public void setTime_h(int time_h) {
		this.time_h = time_h;
	}

	public int getTime_m() {
		return time_m;
	}

	public void setTime_m(int time_m) {
		this.time_m = time_m;
	}

	public int getTime_s() {
		return time_s;
	}

	public void setTime_s(int time_s) {
		this.time_s = time_s;
	}

	

	public long getVideo_id() {
		return video_id;
	}

	public void setVideo_id(long video_id) {
		this.video_id = video_id;
	}

	public long getList_id() {
		return list_id;
	}

	public void setList_id(long list_id) {
		this.list_id = list_id;
	}

	public String getYoutube_id() {
		return youtube_id;
	}

	public void setYoutube_id(String youtube_id) {
		this.youtube_id = youtube_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public String getChannel_title() {
		return channel_title;
	}

	public void setChannel_title(String channel_title) {
		this.channel_title = channel_title;
	}

	public int getStart_time() {
		return start_time;
	}

	public void setStart_time(int start_time) {
		this.start_time = start_time;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public int getTotal_time() {
		return total_time;
	}

	public void setTotal_time(int total_time) {
		this.total_time = total_time;
	}

	public Date getAdd_time() {
		return add_time;
	}

	public void setAdd_time(Date add_time) {
		this.add_time = add_time;
	}

}
