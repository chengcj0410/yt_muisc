package com.lb.ytmusic.model;


import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class YouTubeVideo {

	private String id;
	private String title;
	private String url;
	private String thumbnailUrl;
	private String channelImg;
	private String channelId;
	private String publishDate;
	private String description;
	private String channelTitle;
//	private Map<String, Integer> duration;


//	public Map<String, Integer> getDuration() {
//		return duration;
//	}
//	
//	public void setDuration(Map<String, Integer> map) {
//		this.duration = map;
//	}
	
	public String getChannelImg() {
		return channelImg;
	}
	
	public void setChannelImg(String channelImg) {
		this.channelImg = channelImg;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelTitle() {
		return channelTitle;
	}

	public void setChannelTitle(String channelTitle) {
		this.channelTitle = channelTitle;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
