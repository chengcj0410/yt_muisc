package com.lb.ytmusic.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;

public class LoginInterceptor implements HandlerInterceptor{
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
	
		
		HttpSession session = request.getSession();
		Object loginUser  =session.getAttribute("loginUser");
		
		if(loginUser !=null) {
			return true;
		}
		request.setAttribute("msg", "請先登入");
		request.getRequestDispatcher("/").forward(request, response);
		return false;
	}
}
