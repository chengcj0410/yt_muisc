package com.lb.ytmusic.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.lb.ytmusic.entity.MemberInfo;
import com.lb.ytmusic.model.Member;
import com.lb.ytmusic.service.MemberInfoService;



@Controller
public class LoginController {

	@Autowired
	MemberInfoService memberInfoservice;

	@GetMapping(value = { "/", "/login" })
	public String redirectToLoginPage(Model model) {
		return "login";
	}

	@PostMapping("/login")
	public String loginAccountCheck(Member member, HttpSession session, Model model) {

		MemberInfo findUserName = memberInfoservice.findByAccount(member.getUserAccount());

		if (findUserName != null) {
			if (findUserName.getPassword().equals(member.getPassWord())) {

				Member user_session = new Member();
				user_session.setMemberid(findUserName.getId());
				user_session.setUserAccount(findUserName.getAccount());
				user_session.setEmail(findUserName.getEmail());
				user_session.setUserName(findUserName.getName());

				session.setAttribute("loginUser", user_session);
				return "redirect:/index";
			}
			model.addAttribute("msg", "帳號密碼錯誤");
			return "login";
		}
		model.addAttribute("msg", "無此帳號");
		return "login";
	}

	@PostMapping("/createaccount")
	public String createAccount(Member user, Model model) {

		boolean checkmember = memberInfoservice.save(user.getUserName(), user.getUserAccount(), user.getPassWord(),
				user.getEmail());
		System.out.println(user.getUserAccount());
//		memberInfoservice.save(user.getUserName(), user.getUserAccount(), user.getPassWord(), user.getEmail());
		if (checkmember == false) {
			model.addAttribute("msg", "帳號或名稱已被註冊，請重新註冊");
		} else {

			model.addAttribute("msg", "註冊成功");
		}
		return "login";
	}

}
