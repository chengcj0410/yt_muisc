package com.lb.ytmusic.controller;

import javax.servlet.http.HttpSession;

import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lb.ytmusic.entity.VideoList;
import com.lb.ytmusic.model.VideoListBo;
import com.lb.ytmusic.service.VideoListService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class EditVideoListController {

	@Autowired
	VideoListService videolistservice;

	@GetMapping("/videolist/{id}")
	public String redirectToVideolistPage(@PathVariable("id") Long memberId, Model model, HttpSession session) {
		session.setAttribute("MemberId", memberId);
		Log.info("session user>>>" + memberId);
		return "redirect:/videolist/list/" + memberId;
	}

	@GetMapping("/videolist/list/{id}")
	public String listVideoListTable(@PathVariable("id") Long memberId, Model model,
			@RequestParam(value = "pageNum", defaultValue = "0") int pageNum,
			@RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {

		Page<VideoList> v_list = videolistservice.getPageVideoList(memberId, pageNum, pageSize);

		model.addAttribute("videolist", v_list);
		model.addAttribute("current", v_list.getNumber());
		model.addAttribute("pages", v_list.getTotalPages());
		model.addAttribute("record", v_list.getTotalElements());

		return "tableList/videolist";
	}

	@PostMapping("/create/{id}/videolist")
	public String createVideoList(@PathVariable("id") Long memberId, VideoListBo v_list) {
		Log.info("session >>>" + memberId + "name >>>" + v_list.getListName());

		v_list.setMemberId(memberId);
		videolistservice.add(v_list);

		return "redirect:/videolist/list/" + memberId;
	}

	@PostMapping("/editcomment/{listid}")
	public String editListComment(@PathVariable("listid") Long listId, @RequestParam("comment") String comment,
			HttpSession session, Model model) {

		long memberId = Long.parseLong(session.getAttribute("MemberId").toString());
		videolistservice.editVideoComment(memberId, listId, comment);

		return "redirect:/videolist/list/" + memberId;
	}

	@PostMapping("/deletelist/{listid}")
	public String deleteVideoList(@PathVariable("listid") Long listId, Model model, HttpSession session) {

		long memberId = Long.parseLong(session.getAttribute("MemberId").toString());
		System.out.print("Remove >>ListId ===" + listId);
		videolistservice.remove(listId);

		return "redirect:/videolist/list/" + memberId;
	}

}
