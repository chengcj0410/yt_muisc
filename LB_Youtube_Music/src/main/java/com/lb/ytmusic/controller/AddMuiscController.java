package com.lb.ytmusic.controller;

import javax.servlet.http.HttpSession;

import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lb.ytmusic.entity.VideoInfo;
import com.lb.ytmusic.model.VideoInfoBo;
import com.lb.ytmusic.model.YouTubeVideo;
import com.lb.ytmusic.service.VideoInfoService;
import com.lb.ytmusic.service.VideoListService;
import com.lb.ytmusic.service.YoutubeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class AddMuiscController {
	@Autowired
	private YoutubeService youtubeService;

	@Autowired
	private VideoInfoService videoinfoservice;

	@Autowired
	VideoListService videolistservice;

	@GetMapping("/addmusic/{listid}")
	public String addmusic(@PathVariable("listid") long listid, HttpSession session, Model model) {

		session.setAttribute("listid", listid);

		return "redirect:/addmusic/list/" + listid;
	}

	@GetMapping("/addmusic/list/{listid}")
	public String addmusicpage(@PathVariable("listid") long id, Model model) {

		YouTubeVideo videos = new YouTubeVideo();
		videos.setChannelImg("/img/undraw_profile.svg");

		VideoInfoBo videosetting = new VideoInfoBo();

		Page<VideoInfo> v_list = videoinfoservice.findAllVideoByListIdforPage(id, 0, 5);
		if (v_list != null) {
			model.addAttribute("videoinfo_list", v_list);

		}

		model.addAttribute("listname", videolistservice.findlistnamebyid(id).getListName());
		model.addAttribute("listid", videolistservice.findlistnamebyid(id).getListId());
		model.addAttribute("TimeSetting", videosetting);
		model.addAttribute("Video_Info", videos);
		model.addAttribute("yt_url", "/img/youtubelogo.png");

		return "addmusic";
	}

	@GetMapping("/settablepage")
	public String setTablePage(Model model, HttpSession session,
			@RequestParam(value = "pageNum", defaultValue = "0") int pageNum,
			@RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {

		Log.info("pageNum is " + pageNum);

		long id = Long.parseLong(session.getAttribute("listid").toString());
		Page<VideoInfo> v_list = videoinfoservice.findAllVideoByListIdforPage(id, pageNum, pageSize);
		if (v_list != null) {
			model.addAttribute("videoinfo_list", v_list);
		}
		YouTubeVideo videos = new YouTubeVideo();
		videos.setChannelImg("/img/undraw_profile.svg");

		VideoInfoBo videosetting = new VideoInfoBo();
		model.addAttribute("listname", videolistservice.findlistnamebyid(id).getListName());
		model.addAttribute("listid", videolistservice.findlistnamebyid(id).getListId());
		model.addAttribute("TimeSetting", videosetting);
		model.addAttribute("Video_Info", videos);
		model.addAttribute("yt_url", "/img/youtubelogo.png");

		return "addmusic";
	}

	@PostMapping("/seturl")
	public String setVideoToPreview(HttpSession session, YouTubeVideo videoInfo, Model model) {

		String[] v_url = videoInfo.getUrl().split("=");
		YouTubeVideo videos = youtubeService.fetchVideosByQuery(v_url[1]);
		VideoInfoBo videosetting = new VideoInfoBo();

		long id = Long.parseLong(session.getAttribute("listid").toString());
		Page<VideoInfo> v_list = videoinfoservice.findAllVideoByListIdforPage(id, 0, 5);
		if (v_list != null) {
			model.addAttribute("videoinfo_list", v_list);
		}

		model.addAttribute("listname", videolistservice.findlistnamebyid(id).getListName());
		model.addAttribute("listid", videolistservice.findlistnamebyid(id).getListId());

		videosetting.setTitle(videos.getTitle());
		videosetting.setSinger(videos.getChannelTitle());

		model.addAttribute("TimeSetting", videosetting);
		model.addAttribute("Video_Info", videos);
		model.addAttribute("yt_url", youtubeService.buildVideoUrltoEmbed(v_url[1], 0, 0));

		return "addmusic";
	}

	@PostMapping("/add/{id}/videoinfo/{videoid}")
	public String addVideoInfo(@PathVariable("id") int userId, @PathVariable("videoid") String videoId,
			VideoInfoBo videoInfo, Model model, HttpSession session) {

		int start_time = youtubeService.timeToSecond(videoInfo.getTime_h(), videoInfo.getTime_m(),
				videoInfo.getTime_s());

		videoInfo.setList_id(Long.parseLong(session.getAttribute("listid").toString()));
		videoInfo.setYoutube_id(videoId);
		videoInfo.setStart_time(start_time);
		videoInfo.setTotal_time(videoInfo.getTotal_time() + start_time);
		videoinfoservice.add(videoInfo);

		long id = Long.parseLong(session.getAttribute("listid").toString());
		Page<VideoInfo> v_list = videoinfoservice.findAllVideoByListIdforPage(id, 0, 5);
		if (v_list != null) {
			model.addAttribute("videoinfo_list", v_list);

		}

		model.addAttribute("listname", videolistservice.findlistnamebyid(id).getListName());
		model.addAttribute("listid", videolistservice.findlistnamebyid(id).getListId());

		YouTubeVideo videos = new YouTubeVideo();
		VideoInfoBo videosetting = new VideoInfoBo();
		videos.setChannelImg("/img/undraw_profile.svg");
		model.addAttribute("Video_Info", videos);
		model.addAttribute("TimeSetting", videosetting);
		model.addAttribute("yt_url", "/img/youtubelogo.png");

		return "redirect:/addmusic/" + id;
	}

	@PostMapping("/deletevideo/{videoid}")
	public String deleteVideo(@PathVariable("videoid") String videoId, Model model, HttpSession session) {
		long id = Long.parseLong(session.getAttribute("listid").toString());
		System.out.print("Remove >>videoId ===" + videoId);
		videoinfoservice.remove(Long.parseLong(videoId));

		return "redirect:/addmusic/" + id;
	}

}
