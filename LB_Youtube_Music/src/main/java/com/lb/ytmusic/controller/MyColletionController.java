package com.lb.ytmusic.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.lb.ytmusic.entity.VideoList;
import com.lb.ytmusic.service.VideoListService;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class MyColletionController {

	@Autowired
	VideoListService videolistservice;

	@GetMapping("/mycollection/{id}")
	public String redirectToMyCollectionPage(@PathVariable("id") Long memberid, Model model, HttpSession session) {

		session.setAttribute("MemberId", memberid);
		return "redirect:/mycollection/list/" + memberid;
	}

	@GetMapping("/mycollection/list/{memberid}")
	public String listVideoListTable(@PathVariable("memberid") Long memberId, Model model,
			@RequestParam(value = "pageNum", defaultValue = "0") int pageNum,
			@RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {

		Page<VideoList> v_list = videolistservice.getPageVideoList(memberId, pageNum, pageSize);
		model.addAttribute("videolist", v_list);
		return "mycollection";
	}

	@GetMapping("/mycollection/videolist/{listid}")
	public String myCollectionVideoList(@PathVariable("listid") long listid, HttpSession session, Model model) {

		session.setAttribute("listid", listid);
		return "redirect:/mycollectiontable/videolist/" + listid;
	}

}
