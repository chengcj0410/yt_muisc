package com.lb.ytmusic.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.lb.ytmusic.entity.VideoInfo;
import com.lb.ytmusic.model.VideoInfoBo;
import com.lb.ytmusic.model.YouTubeVideo;
import com.lb.ytmusic.service.VideoInfoService;
import com.lb.ytmusic.service.VideoListService;
import com.lb.ytmusic.service.YoutubeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class MyColletion_videolistController {
	@Autowired
	private YoutubeService youtubeService;

	@Autowired
	VideoListService videolistservice;

	@Autowired
	private VideoInfoService videoinfoservice;

	@GetMapping("/mycollectiontable/videolist/{listid}")
	public String myCollectionVideoListPage(@PathVariable("listid") long id, Model model, HttpSession session) {

		Page<VideoInfo> v_list = videoinfoservice.findAllVideoByListIdforPage(id, 0, 5);
		if (v_list != null) {
			model.addAttribute("videoinfo_list", v_list);
		}

		model.addAttribute("listname", videolistservice.findlistnamebyid(id).getListName());
		model.addAttribute("listid", videolistservice.findlistnamebyid(id).getListId());
		model.addAttribute("Video_Info", new YouTubeVideo());
		model.addAttribute("yt_url", "/img/youtubelogo.png");

		return "tableList/mycollection_videolist";
	}

	@GetMapping("/setmycollectiontablepage")
	public String setTablePage(Model model, HttpSession session,
			@RequestParam(value = "pageNum", defaultValue = "0") int pageNum,
			@RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {

		long id = Long.parseLong(session.getAttribute("listid").toString());
		Page<VideoInfo> v_list = videoinfoservice.findAllVideoByListIdforPage(id, pageNum, pageSize);
		if (v_list != null) {
			model.addAttribute("videoinfo_list", v_list);
		}
		model.addAttribute("listname", videolistservice.findlistnamebyid(id).getListName());
		model.addAttribute("listid", videolistservice.findlistnamebyid(id).getListId());
		model.addAttribute("TimeSetting", new VideoInfoBo());
		model.addAttribute("Video_Info", new YouTubeVideo());
		model.addAttribute("yt_url", "/img/youtubelogo.png");

		return "tableList/mycollection_videolist";
	}

	@GetMapping("/setvideoinfo")
	public String setVideoInfoForIframe(@RequestParam("v_id") long video_id, Model model, HttpSession session) {
		System.out.print("setVideoInfoForIframe video id is " + video_id);
		VideoInfoBo videoinfobo = videoinfoservice.findVideoById(video_id);
		YouTubeVideo videos = youtubeService.fetchVideosByQuery(videoinfobo.getYoutube_id());

		long id = Long.parseLong(session.getAttribute("listid").toString());
		Page<VideoInfo> v_list = videoinfoservice.findAllVideoByListIdforPage(id, 0, 5);
		if (v_list != null) {
			model.addAttribute("videoinfo_list", v_list);
		}
		model.addAttribute("yt_url", youtubeService.buildVideoUrltoEmbed(videoinfobo.getYoutube_id(),
				videoinfobo.getStart_time(), videoinfobo.getTotal_time()));
		model.addAttribute("Video_Info", videos);
		return "tableList/mycollection_videolist";
	}

}
