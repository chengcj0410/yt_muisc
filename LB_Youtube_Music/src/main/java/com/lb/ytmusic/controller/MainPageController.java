package com.lb.ytmusic.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lb.ytmusic.entity.VideoInfo;
import com.lb.ytmusic.model.VideoInfoBo;
import com.lb.ytmusic.service.VideoInfoService;
import com.lb.ytmusic.service.VideoListService;
import com.lb.ytmusic.service.YoutubeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class MainPageController {
	@Autowired
	private YoutubeService youtubeService;

	@Autowired
	VideoListService videolistservice;

	@Autowired
	private VideoInfoService videoinfoservice;

	@GetMapping("/index")
	public String indexPage(HttpSession session, Model model) {
		return "redirect:/index/list";
	}

	@GetMapping("/index/list")
	public String listByPage(Model model, @RequestParam(value = "pageNum", defaultValue = "0") int pageNum,
			@RequestParam(value = "pageSize", defaultValue = "5") int pageSize) {

	
		Page<VideoInfo> v_info = videoinfoservice.findAllVideo(pageNum, pageSize);

		if (v_info != null) {
			model.addAttribute("videolist", v_info);
		}
		model.addAttribute("Video_Info", new VideoInfoBo());
		model.addAttribute("yt_url", "/img/youtubelogo.png");
		model.addAttribute("channelImg", "/img/undraw_profile.svg");

		return "index";
	}

	@GetMapping("/indexsetvideoinfo")
	public String setVideoInfoToIframe(@RequestParam("v_id") long video_id, Model model) {

		VideoInfoBo videoinfobo = videoinfoservice.findVideoById(video_id);
		Page<VideoInfo> v_info = videoinfoservice.findAllVideo(0, 5);

		if (v_info != null) {
			model.addAttribute("videolist", v_info);
		}
		model.addAttribute("yt_url", youtubeService.buildVideoUrltoEmbed(videoinfobo.getYoutube_id(),
				videoinfobo.getStart_time(), videoinfobo.getTotal_time()));
		model.addAttribute("Video_Info", videoinfobo);

		return "index";
	}
}
