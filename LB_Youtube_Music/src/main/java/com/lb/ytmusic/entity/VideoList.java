package com.lb.ytmusic.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "video_list")
public class VideoList  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -701423003094569377L;
	
	
	@Id
	@Column(name = "list_id")
	private long listId;	
	@Column(name = "member_id")
	private long memberId;
	@Column(name = "list_name")
	private String listName;
	@Column(name = "create_at")
	private Date create_at;
	@Column(name = "update_at")
	private Date update_at;
	@Column(name = "list_comment")
	private String list_comment;
	
	@ManyToOne
	@JoinColumn(name = "member_id", insertable = false, updatable = false)
	private MemberInfo memberinfo ;

	@OneToMany(cascade=CascadeType.ALL,mappedBy = "vl")
	private List<VideoInfo> vif;

	public String getList_comment() {
		return list_comment;
	}
	public void setList_comment(String list_comment) {
		this.list_comment = list_comment;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	
	public long getListId() {
		return listId;
	}
	public void setListId(long listId) {
		this.listId = listId;
	}
	public String getListName() {
		return listName;
	}
	public void setListName(String listName) {
		this.listName = listName;
	}
	public Date getCreate_at() {
		return create_at;
	}
	public void setCreate_at(Date create_at) {
		this.create_at = create_at;
	}
	public Date getUpdate_at() {
		return update_at;
	}
	public void setUpdate_at(Date update_at) {
		this.update_at = update_at;
	}

}
