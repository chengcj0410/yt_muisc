package com.lb.ytmusic.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.ToString;


/**
 * 
 * @author root
 * @DB member
 * @table/videoinfo
 *
 */
@ToString
@Entity
@Table(name = "video_info")
public class VideoInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "video_id")
	private long video_id;

	@ManyToOne
	@JoinColumn(name = "list_id", insertable = false, updatable = false)
	private VideoList vl;
	

	@Column(name = "list_id")
	private long list_id;
	@Column(name = "title")
	private String title;
	@Column(name = "youtube_id")
	private String youtube_id;
	@Column(name = "singer")
	private String singer;
	@Column(name = "channel_title")
	private String channel_title;
	@Column(name = "start_time")
	private int start_time;
	@Column(name = "total_time")
	private int total_time;
	@Column(name = "add_time")
	private Date add_time;
	@Column(name = "thumbnail_url")
	private String thumbnailUrl;

	public long getVideo_id() {
		return video_id;
	}

	public void setVideo_id(long video_id) {
		this.video_id = video_id;
	}

	public long getList_id() {
		return list_id;
	}

	public void setList_id(long l) {
		this.list_id = l;
	}

	public String getYoutube_id() {
		return youtube_id;
	}

	public void setYoutube_id(String youtube_id) {
		this.youtube_id = youtube_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSinger() {
		return singer;
	}

	public void setSinger(String singer) {
		this.singer = singer;
	}

	public String getChannel_title() {
		return channel_title;
	}

	public void setChannel_title(String channel_title) {
		this.channel_title = channel_title;
	}

	public int getStart_time() {
		return start_time;
	}

	public void setStart_time(int start_time) {
		this.start_time = start_time;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}

	public int getTotal_time() {
		return total_time;
	}

	public void setTotal_time(int total_time) {
		this.total_time = total_time;
	}

	public Date getAdd_time() {
		return add_time;
	}

	public void setAdd_time(Date add_time) {
		this.add_time = add_time;
	}

}
