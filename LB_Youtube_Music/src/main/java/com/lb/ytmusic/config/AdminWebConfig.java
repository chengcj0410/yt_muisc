package com.lb.ytmusic.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.lb.ytmusic.interceptor.LoginInterceptor;

@Configuration
public class AdminWebConfig implements WebMvcConfigurer{
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	registry.addInterceptor(new LoginInterceptor())
	.addPathPatterns("/**")
	.excludePathPatterns("/","/login","/createaccount","/css/**","/img/**","/js/**","/vendor/**");
		
	}

}
