package com.lb.ytmusic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YoutubeMusicApplication {

	public static void main(String[] args) {
		SpringApplication.run(YoutubeMusicApplication.class, args);
	}

}
