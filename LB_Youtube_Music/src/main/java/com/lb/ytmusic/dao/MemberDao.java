package com.lb.ytmusic.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.lb.ytmusic.entity.MemberInfo;

/**
 * 
 * @author handle DB information by SQL
 *
 */
@Repository
public interface MemberDao extends JpaRepository<MemberInfo, Long> {

	List<MemberInfo> findAll();

	List<MemberInfo> findByEmail(String email);

	List<MemberInfo> findByName(String name);

	MemberInfo findByAccount(String account);

	List<MemberInfo> findAllById(Long id);


}
