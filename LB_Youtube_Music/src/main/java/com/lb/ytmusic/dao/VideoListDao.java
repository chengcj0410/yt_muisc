package com.lb.ytmusic.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import com.lb.ytmusic.entity.VideoList;

@Repository

public interface VideoListDao extends JpaRepository<VideoList, Long> {

	@Query("SELECT uc FROM VideoList uc WHERE uc.memberId = ?1")
	List<VideoList> findlistbyid(Long memberId);

	@Query("SELECT listname FROM VideoList listname WHERE listname.listId = ?1")
	VideoList findlistnamebyid(Long listid);

	@Query("SELECT uc FROM VideoList uc WHERE uc.memberId = ?1")
	Page<VideoList> findListbyMemberID(Long memberId, Pageable pageable);

}
