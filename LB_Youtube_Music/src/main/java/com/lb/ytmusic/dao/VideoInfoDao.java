package com.lb.ytmusic.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lb.ytmusic.entity.VideoInfo;


@Repository
public interface VideoInfoDao extends JpaRepository<VideoInfo, Long> {

//	List<VideoInfo> findAll();
	
	@Query("SELECT uc FROM VideoInfo uc WHERE uc.list_id = ?1")
	List<VideoInfo> findAllVideoByListId(Long list_id);
	
	@Query("SELECT uc FROM VideoInfo uc WHERE uc.list_id = ?1")
	Page<VideoInfo> findAllVideoByListId(Long list_id,Pageable pageable);
	
	@Query("SELECT uc FROM VideoInfo uc WHERE uc.video_id = ?1")
	VideoInfo findVideoById(Long video_id);
	
	@Query("SELECT uc FROM VideoInfo uc ORDER BY uc.add_time ASC")
	Page<VideoInfo> findAllVideo(Pageable pageable);
	
}
