

$(document).ready(function() {

	/** 
	  * reset form of setTime
	**/
	$("#resetTimeForm").click(function() {
		$("#TimeForm_hour").val(0);
		$("#TimeForm_min").val(0);
		$("#TimeForm_sec").val(0);
		$("#TimeForm_total").val(0);
		console.log("clear");
	});
	/** 
	  * preview the video by set time
	**/
	$("#getVideoUrl").click(function() {
		var url = $("#videoUrl").val();
		//console.log('geturl' + url);
		if (url == "") {
			alert("請輸入影片網址");
		} else {
			var urlarray = url.split("=");
			var startTime = roughScale($("#TimeForm_hour").val()) * 3600 + roughScale($("#TimeForm_min").val()) * 60 + roughScale($("#TimeForm_sec").val());
			//console.log('starttime' + startTime);
			var totalTime = startTime + roughScale($("#TimeForm_total").val());
			var embed = "https://www.youtube.com/embed/" + urlarray[1] + "?start=" + startTime + "&end=" + totalTime + "&autoplay=1";
			//console.log('embedUrl' + embed);
			$("#iframe_video").attr("src", embed);
		}
	});
	//check timeform setting not be empty
	$("#addvideoinfo").click(function() {
		
		console.log(">>>>"+$("#iframe_video").attr("src"));
		
		
		//if($("#iframe_video").getElementsByTagName("src"))
		
		if ($("#videoUrl").val() == "") {
			alert("影片URL未填寫");
			return false;
		} else {
			const hour = $("#TimeForm_hour").val();
			const min = $("#TimeForm_min").val();
			const sec = $("#TimeForm_sec").val();
			const total = $("#TimeForm_total").val();
			console.log("h:" + hour + " m:" + min + " s:" + sec + " total:" + total);


			if (hour == "") {
				$("#TimeForm_hour").val(0);
			}
			if (min == "") {
				$("#TimeForm_min").val(0);
			}
			if (sec == "") {
				$("#TimeForm_sec").val(0);
			}
			if (total == "") {
				$("#TimeForm_total").val(0);
			}
		}

		if($("#iframe_video").attr("src")=="/img/youtubelogo.png"){
			alert("請先預覽影片並設定起始時間");
			return false;
		}
	});



	$("#collapse_button").click(function() {
		$("#collapseOne").slideToggle("slow");
	});


});


/** 
  * Change String to integer by base 10
**/
function roughScale(x) {
	const parsed = parseInt(x, 10);
	if (isNaN(parsed)) { return 0; }
	return parsed;
}
//Count the String to byte.
String.prototype.Blength = function() {
	var arr = this.match(/[^\x00-\xff]/ig);
	return arr == null ? this.length : this.length + arr.length;
}

function setComment(id) {
	//Video List Comment Dialog 	
	//add a comment for videolist
	$("#list_comment_dialog").dialog({
		height: 250,
		width: 400,
		autoOpen: false,
		modal: true,
		buttons:
		{
			"確認": function() {
				if ($("#list_comment").val().length > 50) {
					alert("內容長度過多，請刪減");
				} else {
					//console.log(id);
					$.post("/editcomment/" + id, { comment: $("#list_comment").val() })
				}
				$(this).dialog("close");
			},
			"取消": function() {
				$(this).dialog("close");
			}
		}

	});
	$("#list_comment_dialog").dialog("open");

}


